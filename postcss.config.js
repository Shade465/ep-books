module.exports = {
    plugins: {
        autoprefixer: {},
        'postcss-custom-media': {
            importFrom: ['./src/assets/css/media.css']
        },
        'postcss-color-mod-function': {},
        'postcss-preset-env': {
            stage: 0,
            browsers: 'cover 90%, last 2 major versions'
        }
    }
}
