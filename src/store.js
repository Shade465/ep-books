import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        books: [
            {
                id: 1,
                name: 'Клатбище домашних жывотных',
                author: 'Стивен Кинг',
                year: '1983',
                image:
                    'https://cdn.book24.ru/v2/ASE000000000706477/COVER/cover3d1__w337.jpg'
            },
            {
                id: 2,
                name: 'Оно',
                author: 'Стивен Кинг',
                year: '1986',
                image:
                    'https://cdn.book24.ru/v2/AST000000000029614/COVER/cover3d1__w674.jpg'
            },
            {
                id: 3,
                name: 'Худеющий',
                author: 'Стивен Кинг',
                year: '1984',

                image:
                    'https://cdn.book24.ru/v2/AST000000000138327/COVER/cover3d1__w337.jpg'
            },
            {
                id: 4,
                name: 'Ветер сквозь замочную скважину',
                author: 'Стивен Кинг',
                year: '2012',
                image:
                    'https://cdn.book24.ru/v2/ASE000000000708272/COVER/cover3d1__w337.jpg'
            },
            {
                id: 5,
                name: 'Сияние',
                author: 'Стивен Кинг',
                year: '1977',
                image:
                    'https://cdn.book24.ru/v2/AST000000000132445/COVER/cover3d1__w337.jpg'
            }
        ]
    },
    getters: {
        getBookById(state) {
            return id => {
                return state.books.find(book => book.id === id * 1)
            }
        },
        getBooks(state) {
            return state.books.sort((a, b) => b.id - a.id)
        }
    },
    mutations: {
        ADD(state, book) {
            state.books.unshift(book)
        },
        DELETE(state, id) {
            const index = state.books.findIndex(book => book.id === id)
            state.books.splice(index, 1)
        },
        EDIT(state, book) {
            state.books = state.books.map(el => {
                if (el.id === book.id) {
                    el = book
                }
                return el
            })
        }
    },
    actions: {
        addBook({ state, commit }, book) {
            const newBook = { id: state.books.length + 1, ...book }
            commit('ADD', newBook)
            router.push('/')
        },
        deleteBook({ commit }, id) {
            commit('DELETE', id)
        },
        editBook({ commit }, book) {
            commit('EDIT', book)
            router.push('/')
        }
    },
    plugins: [createPersistedState()]
})
