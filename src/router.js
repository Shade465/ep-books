import Vue from 'vue'
import Router from 'vue-router'
import Index from './pages/Index'

Vue.use(Router)

const routes = [
    { path: '/', name: 'index', component: Index },
    {
        path: '/add',
        name: 'add',
        component: () => import(/* webpackChunkName: "add" */ './pages/Add')
    },
    {
        path: '/edit/:id',
        name: 'edit',
        component: () => import(/* webpackChunkName: "edit" */ './pages/_Edit')
    }
]

const router = new Router({
    mode: 'history',
    linkActiveClass: 'active',
    linkExactActiveClass: 'active',
    base: process.env.NODE_ENV === 'production' ? '/ep-books' : '/',
    routes,
    scrollBehavior() {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve({ x: 0, y: 0 })
            }, 350)
        })
    }
})

export default router
